#### Binvenido, Gracias por Visitar 🤚💻

# Hola! 👋 Soy Rafael Arrieta Salcedo
* Ingeniero de Sistemas - Fullstack developer
* Siempre en Constante aprendizaje,
* me encanta aprender y enfrentarme a nuevos desafíos.
La base de todo: Mantenerse en movimiento 🚀

###  📌  sobre mi:
Soy un profesional con aptitud para el
trabajo en equipo, liderazgo, buenas
relaciones interpersonales, toma de
decisiones. Con facilidad de desarrollar
habilidades y cualidades que me permiten
enfrentar eficientemente a actividades
propias de mi profesión.


![rafa_perfil](/uploads/e8d8d4c2904473207b2cd319cbdd75fb/rafa_perfil.gif)



<!--
*ralh17/ralh17* is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitLab profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->

js
const ralh17 = {
  pronouns: "he" | "him",
  code: [Javascript, HTML, CSS, SCSS],
  tools: [React, Redux, Node JS, Docker, Docker Compose, PHP, VUE, Socket IO, WEBRTC, MongoDB, Sequelize, Javascript Vanilla, Firebase, MySQL, Google Cloud Platform],
